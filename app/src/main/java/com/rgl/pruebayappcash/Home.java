package com.rgl.pruebayappcash;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Home extends Fragment {

    View vista;
    TextView txvlbl;
    Usuario usuario;
    Lista_Datos [] lista;
    FloatingActionButton fab;
    FragmentTransaction fragmentTransaction;

    final int PERMISSION_ALL = 112;
    String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.INTERNET,
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista=inflater.inflate(R.layout.fragment_home, container, false);

        usuario=((MainActivity)getActivity()).usuario;

        txvlbl = vista.findViewById(R.id.txvlbl);
        txvlbl.setText("Bienvenido: "+usuario.getUsuario());
        fab= vista.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTransaction = ((MainActivity)getActivity()).fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frag_contenedor,new Mapa_Fragment());
                fragmentTransaction.commit(); 
            }
        });

        permisos();
        consulta_lista();
        return vista;
    }

    void permisos(){
        if(!hasPermissions(getActivity(), PERMISSIONS)){
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(getActivity(), "Permisos denegados", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    void consulta_lista(){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = "http://3.94.51.72/skillTest/testContent";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject res) {
                        Log.e("Res",res.toString());
                        try {
                            JSONObject data = res.getJSONObject("data");
                            JSONArray listajson = data.getJSONArray("exampleList");
                            lista= new Lista_Datos[listajson.length()];
                            for (int i = 0; i < listajson.length(); i++){
                                JSONObject json_item= listajson.getJSONObject(i);
                                lista[i]= new Lista_Datos(json_item.getString("title"),json_item.getString("content"));
                            }

                            RecyclerView recyclerView = vista.findViewById(R.id.rcview);
                            ListAdapter adapter = new ListAdapter(lista);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error volley",error.toString());
                    }
                });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjectRequest);
    }
}