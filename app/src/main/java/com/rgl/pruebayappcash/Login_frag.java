package com.rgl.pruebayappcash;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login_frag extends Fragment {

    View vista;
    Button btsesion ;
    EditText edtusuario,edtcontra;
    Usuario usuario;
    FragmentTransaction fragmentTransaction;
    RequestQueue queue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista=inflater.inflate(R.layout.fragment_login_frag, container, false);

        btsesion = vista.findViewById(R.id.btsesion);
        edtusuario = vista.findViewById(R.id.edtusuario);
        edtcontra = vista.findViewById(R.id.edtcontrasenia);

        btsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iniciar_sesion();
            }
        });

        return vista;
    }

    void iniciar_sesion(){
        usuario = new Usuario();
        usuario.setUsuario(edtusuario.getText().toString().trim());
        usuario.setContrasenia(edtcontra.getText().toString().trim());

        final String url = "http://3.94.51.72/skillTest/testLogin";
        queue = Volley.newRequestQueue(getActivity());
        JSONObject object = new JSONObject();
        try {
            //input your API parameters
            object.put("user",usuario.getUsuario());
            object.put("password",usuario.getContrasenia());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("","String Response : "+ response.toString());
                        try {
                            if(response.getBoolean("error")==false){
                                ((MainActivity)getActivity()).usuario=usuario;

                                fragmentTransaction = ((MainActivity)getActivity()).fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frag_contenedor,new Home());
                                fragmentTransaction.commit();
                            }else
                                mensaje("Alerta",response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error",error.toString());
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjectRequest);


    }

    void mensaje(String titulo,String msj){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(titulo);
        builder.setMessage(msj);
        builder.setPositiveButton("Aceptar", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}