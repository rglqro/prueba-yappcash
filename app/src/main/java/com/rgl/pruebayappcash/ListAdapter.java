package com.rgl.pruebayappcash;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    Lista_Datos lista[];

    public ListAdapter(Lista_Datos[] lista){
        this.lista=lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.lista_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txvtitulo.setText(lista[position].getTitle());
        holder.txvcontenido.setText(lista[position].getContent());
    }

    @Override
    public int getItemCount() {
        return lista.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txvtitulo,txvcontenido;
        public LinearLayout ll;
        public ViewHolder(View itemView) {
            super(itemView);
            this.txvtitulo =itemView.findViewById(R.id.txvtitulo);
            this.txvcontenido =itemView.findViewById(R.id.txvcontenido);
            ll = (LinearLayout)itemView.findViewById(R.id.ll);
        }
    }
}
